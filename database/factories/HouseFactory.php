<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class HouseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'House-'.$this->faker->randomNumber(),
            'owner' => $this->faker->name,
            'price' => $this->faker->numberBetween(100000, 20000000)
        ];
    }
}
