<?php

namespace Tests\Feature;

use App\Models\House;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class HouseTest extends TestCase
{
    /**
     * Testing if it stores house successfully.
     *
     * @return void
     */
    public function test_store_house()
    {
        $houseModel = new House();
        $postUrl = '/api/house';

        $data = $houseModel->factory()->make()->toArray();

        $this->post($postUrl, $data);

        $this->assertDatabaseHas($houseModel->getTable(), $data);
    }

    /**
     * Testing if it fetches house successfully.
     *
     * @return void
     */
    public function test_fetch_all_houses()
    {
        $response = $this->get('/api/house');

        $response->assertStatus(200);
    }
}
