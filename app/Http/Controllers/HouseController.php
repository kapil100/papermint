<?php

namespace App\Http\Controllers;

use App\Repositories\HouseRepositories;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    private $houseRepositories;

    public function __construct(HouseRepositories $houseRepositories)
    {
        $this->houseRepositories = $houseRepositories;
    }

    public function index(Request $request){
        $params = $request->toArray();
        return response(
            $this->houseRepositories->index($params),
            200
        );
    }

    public function save(Request $request){
        $request->validate([
            'name' => 'required',
            'owner' => 'required',
            'price' => 'required|min:100000'
        ]);

        return response(
            $this->houseRepositories->save($request->only(['name', 'owner', 'price'])),
            200
        );
    }

}
