<?php


namespace App\Repositories;


use App\Models\House;

class HouseRepositories
{
    private $house;

    public function __construct(House $house)
    {
        $this->house = $house;
    }

    /**
     * Get all data from house table which matches the given params
     * if params is empty it will return all the houses
     * @param array $params
     */
    public function index($params = []){
        $query = $this->house->query();

        //TODO

        return $query->get();
    }

    /**
     * saves data into house table
     * @param $data
     */
    public function save($data){

        return $this->house->create([
            'name' => $data['name'],
            'owner' => $data['owner'],
            'price' => $data['price'],
        ]);
    }
}
